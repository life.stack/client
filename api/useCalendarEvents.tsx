import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { isArray } from '@chakra-ui/utils'
import { isPlainObject } from 'lodash'

const useCalendarEvents = (
  calendarId,
  initialCalendarEvents?,
  initialCalendar?
) => {
  const { mutate: swrMutate } = useSWRConfig()
  const toast = useToast()
  const router = useRouter()
  const {
    data: events,
    error: eventsError,
    mutate: eventsMutate,
  } = useSWR(`/api/calendars/${calendarId}/events`, {
    fallbackData: initialCalendarEvents,
  })
  const {
    data: calendar,
    error: calendarError,
    mutate: calendarMutate,
  } = useSWR(`/api/calendars/${calendarId}`, { fallbackData: initialCalendar })

  const { doRequest, errors } = useRequest({})

  // console.log('useCalendarEvents', events, errors)

  const updateCalendarEvent = async ({ event }) => {
    console.log(
      'updateCalendarEvent',
      event.toPlainObject({ collapseExtendedProps: true })
    )
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'put',
      body: event.toPlainObject({ collapseExtendedProps: true }),
      onSuccess: (data) => {
        console.log('updateCalendarEvent.success', data)
        eventsMutate(events.map((e) => (e.id === event.id ? data : e)))
      },
    })
  }

  const removeCalendarEvent = async (event) => {
    console.log('removeCalendarEvent', event)
    await doRequest({
      url: `/api/calendars/${calendar.id}/events/${event.id}`,
      method: 'delete',
      onSuccess: (data) => {
        console.log('removeCalendarEvent.success', data)
        eventsMutate(events.filter((e) => e.id === event.id))
      },
    })
  }

  return {
    events: isArray(events) ? events : [],
    calendar: isPlainObject(calendar) ? calendar : {},
    updateCalendarEvent,
    removeCalendarEvent,
    isLoading: !eventsError && !calendarError && !events,
    isError: eventsError || calendarError,
  }
}

export default useCalendarEvents
