import useSWR from 'swr'
import { isPlainObject } from 'lodash'
import useRequest from '../hooks/useRequest'

const useCalendar = (calendarId, initialCalendar) => {
  const {
    data: calendar,
    error: calendarError,
    mutate: calendarMutate,
  } = useSWR(`/api/calendars/${calendarId}`, { fallbackData: initialCalendar })

  return {
    calendar: isPlainObject(calendar) ? calendar : {},
    isLoading: !calendarError && !calendar,
    isError: calendarError,
  }
}

export default useCalendar
