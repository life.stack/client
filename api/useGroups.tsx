import { useToast } from '@chakra-ui/react'
import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import { createContext, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { useUsers } from './useUsers'
import { SWRResponse } from 'swr/dist/types'

export const GroupsContext = createContext({} as useGroups)

type useGroups = {
  groups: [
    {
      id: string
      name: string
    }?
  ]
  isLoading: boolean
  isError: boolean
  createGroup: Function
  inviteCode: string
  generateInvite: Function
  acceptGroup: Function
  clearGroups: Function
}

export const useGroups = (): useGroups => useContext(GroupsContext) as useGroups

export const GroupsProvider = ({ children }) => {
  const toast = useToast()
  const { user } = useUsers()
  const {
    data: groups,
    error,
    mutate,
  }: SWRResponse<
    [{ id: string; name: string }?],
    [{ message: string }]
  > = useSWR('/api/groups/currentGroups', {
    fallbackData: [],
    revalidateOnReconnect: user !== null,
    revalidateOnFocus: user !== null,
    revalidateOnMount: user !== null,
    shouldRetryOnError: user !== null,
  })
  const [inviteCode, setInviteCode] = useState('')
  const router = useRouter()

  console.log('GroupsProvider', groups, error)

  // if (
  //   //TODO: find the best spot to push users towards group creation/joining
  //   global.window &&
  //   user !== null &&
  //   groups.length === 0 &&
  //   !router.pathname.startsWith('/groups')
  // ) {
  //   toast({
  //     title: 'Create or Join a Group!',
  //     description:
  //       'You must join an existing group or create your own in order to begin Life.Stack',
  //     duration: 10000,
  //     isClosable: true,
  //   })
  //   router.push('/groups')
  // }

  const { doRequest: createRequest } = useRequest({
    url: '/api/groups',
    method: 'post',
    onSuccess: async (createdGroup) => {
      toast({
        title: 'Group Created! 🎉',
      })
      await mutate([createdGroup], true)
      await router.push('/')
    },
  })

  const createGroup = async ({ name }) => {
    console.log('Creating Group', name)
    await createRequest({ body: { name } })
  }

  const joinGroup = () => {}

  const { doRequest: requestInvite } = useRequest({
    url: `/api/groups/${groups[0]?.id}/invite`,
    method: 'post',
    onSuccess: (data) => {
      setInviteCode(data?.inviteKey)
      toast({
        title: 'Invite Code Generated',
      })
    },
  })

  const generateInvite = async () => {
    console.log('Generating invite...')
    const response = await requestInvite()
  }

  const { doRequest: acceptRequest } = useRequest({
    url: `/api/groups/accept`,
    method: 'post',
    onSuccess: async (response) => {
      toast({
        title: 'Invite Accepted!',
      })
      console.log('accepted group', response)
      await mutate([response], true)
      await router.push('/')
    },
  })

  const acceptGroup = async ({ inviteKey }) => {
    console.log('Accepting Group...')
    await acceptRequest({ body: { inviteKey } })
  }

  const clearGroups = async () => {
    await mutate([], true)
  }

  return (
    <GroupsContext.Provider
      value={{
        groups,
        isLoading: !error && !groups,
        isError: Boolean(error),
        createGroup,
        inviteCode,
        generateInvite,
        acceptGroup,
        clearGroups,
      }}
    >
      {children}
    </GroupsContext.Provider>
  )
}
