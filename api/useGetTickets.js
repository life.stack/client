import useSWR from 'swr'

const useGetTickets = () => {
  const { data, error } = useSWR('/api/tickets')
  console.log('useGetTickets.data', data, error)

  return {
    tickets: data,
    isLoading: !error && !data,
    isError: error,
  }
}

export default useGetTickets
