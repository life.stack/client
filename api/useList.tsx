import useSWR, { useSWRConfig } from 'swr'
import useRequest from '../hooks/useRequest'
import { useState } from 'react'
import { useToast } from '@chakra-ui/react'
import { useRouter } from 'next/router'

const useList = (listId) => {
  const { mutate: swrMutate } = useSWRConfig()

  const toast = useToast()
  const router = useRouter()
  const [showChecked, setShowChecked] = useState(false)
  const { data: list, error, mutate } = useSWR(`/api/lists/${listId}`)

  const { doRequest } = useRequest({})

  const appendItem = async ({ title }) => {
    console.log('appendItem', title)
    await doRequest({
      body: { title: title },
      url: `/api/lists/${listId}`,
      method: 'post',
      onSuccess: async (appendedItem) => {
        await mutate({ ...list, items: [...list.items, appendedItem] }, true)
      },
    })
  }

  const toggleShowChecked = () => {
    console.log('toggleShowChecked', showChecked)
    setShowChecked(!showChecked)
  }

  const updateItem = async ({ item, itemId }) => {
    console.log('updateItemRequest', item)
    await doRequest({
      url: `/api/items/${itemId}`,
      body: {
        title: item.title,
        note: item.note,
        checked: item.checked,
        subitems: item.subitems,
      },
      method: 'put',
      onSuccess: async (updatedItem) => {
        await mutate(
          {
            ...list,
            items: list.items.map((item) =>
              item.id === updatedItem.id ? updatedItem : item
            ),
          },
          false
        )
      },
    })
  }

  const toggleItemChecked = async ({ id, checked }) => {
    await mutate(
      {
        ...list,
        items: list.items.map((item) =>
          item.id === id ? { ...item, checked: !checked } : item
        ),
      },
      false
    )

    await doRequest({
      url: `/api/items/${id}/checked`,
      body: {
        checked: !checked,
      },
      method: 'put',
      onSuccess: async (updatedItem) => {
        await mutate(
          {
            ...list,
            items: list.items.map((item) =>
              item.id === updatedItem.id ? updatedItem : item
            ),
          },
          false
        )
      },
    })
  }

  const toggleSubitemChecked = async ({ itemId, subitemId, checked }) => {
    await mutate(
      {
        ...list,
        items: list.items.map((item) =>
          item.id === itemId
            ? {
                ...item,
                subitems: item.subitems.map((subitem) =>
                  subitem.id === subitemId
                    ? { ...subitem, checked: !checked }
                    : subitem
                ),
              }
            : item
        ),
      },
      false
    )

    await doRequest({
      method: 'put',
      url: `/api/subitems/${subitemId}/checked`,
      body: {
        checked: !checked,
      },
    })
  }

  const deleteList = async () => {
    await doRequest({
      method: 'delete',
      url: `/api/lists/${listId}`,
      onSuccess: () => {
        swrMutate('/api/lists')
        router.push('/')
        toast({
          title: 'List Deleted',
          variant: 'solid',
          status: 'warning',
        })
      },
    })
  }

  const filteredListItems =
    list?.items?.filter((item) => {
      item.filteredSubitems = item.subitems.filter(
        (subitem) => !subitem.checked || showChecked
      )
      return !item.checked || showChecked
    }) || []

  console.log('useList', list, filteredListItems, showChecked)

  return {
    list,
    filteredListItems,
    showChecked,
    setShowChecked,
    toggleShowChecked,
    isLoading: !error && !list,
    isError: error,
    appendItem,
    updateItem,
    toggleItemChecked,
    toggleSubitemChecked,
    deleteList,
  }
}

export default useList
