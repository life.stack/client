import useSWR from 'swr'
import useRequest from '../hooks/useRequest'
import Router, { useRouter } from 'next/router'
import { useToast } from '@chakra-ui/react'
import { createContext, useContext } from 'react'

export const UsersContext = createContext({} as UsersContextType)

export type UsersContextType = {
  user: { id: string; email: string; groups: [string] }
  isLoading: boolean
  isError: boolean
  signin: (props: { email: string; password: string }) => Promise<void>
  signup: (props: { email: string; password: string }) => Promise<void>
  signout: () => Promise<void>
}

export const useUsers = () => useContext(UsersContext)

export const UsersProvider = ({ children }) => {
  const toast = useToast()
  const router = useRouter()
  const { data, error, mutate } = useSWR('/api/users/currentuser', {
    onError: () => {
      console.warn('UsersProvider.onError -- signing out')
      // router.push('/auth/signout')
    },
  })

  console.log('UserProvider', data)

  const { doRequest: signinRequest, errors: signinError } = useRequest({
    url: '/api/users/signin',
    method: 'post',
    onSuccess: () => {
      toast({
        title: 'Welcome Back!',
      })
      Router.push('/')
    },
  })

  const { doRequest: signoutRequest, errors: signoutErrors } = useRequest({
    url: '/api/users/signout',
    method: 'post',
    body: {},
    onSuccess: () => {
      Router.push('/')
    },
  })

  const { doRequest: signupRequest, errors: signupErrors } = useRequest({
    url: '/api/users/signup',
    method: 'post',
    onSuccess: () => {
      toast({
        title: 'What better place than here? what better time than now!',
      })
      Router.push('/groups')
    },
  })

  const signup = async ({ email, password }) => {
    await signupRequest({ body: { email, password } })
    await mutate({ currentUser: null }, true)
  }

  const signout = async () => {
    await signoutRequest()
    await mutate({ currentUser: null }, true)
  }

  const signin = async ({ email, password }) => {
    const user = await signinRequest({ body: { email, password } })
    await mutate({ currentUser: user }, true)
  }

  return (
    <UsersContext.Provider
      value={{
        user: data?.currentUser,
        isLoading: !error && !data,
        isError: error || signinError || signupErrors || signoutErrors,
        signout: signout,
        signin: signin,
        signup: signup,
      }}
    >
      {children}
    </UsersContext.Provider>
  )
}
