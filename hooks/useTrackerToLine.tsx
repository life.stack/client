import { sortBy, minBy, maxBy } from 'lodash'
import { intervalToDuration } from 'date-fns'
import { useMediaQuery } from '@chakra-ui/react'

const useTrackerToLine = ({ events, name }) => {
  const eventData = sortBy(
    events.map((event) => {
      return {
        x: new Date(event.timestamp),
        y: event.value,
      }
    }),
    'x'
  )

  const data = [
    {
      id: name,
      data: eventData,
    },
  ]

  const minDate = minBy(eventData, 'x')
  const maxDate = maxBy(eventData, 'x')
  const dateDuration = intervalToDuration({
    start: minDate?.x || new Date(),
    end: maxDate?.x || new Date(),
  })

  let dateTickValues = undefined
  let axisBottomFormat = '%b %d %Y'

  if (dateDuration.years > 1) {
    dateTickValues = 'every year'
    axisBottomFormat = '%b %Y'
  } else if (dateDuration.months >= 6) {
    dateTickValues = 'every month'
    axisBottomFormat = '%b %Y'
  } else if (dateDuration.months >= 2) {
    dateTickValues = 'every week'
    axisBottomFormat = '%b %Y'
  } else if (dateDuration.days >= 14) {
    dateTickValues = 'every 5 days'
  } else if (dateDuration.days >= 7) {
    dateTickValues = 'every 2 days'
  } else {
    dateTickValues = 'every day'
  }

  console.log('useTrackerToLine', {
    dateDuration,
    dateTickValues,
    axisBottomFormat,
  })

  return { data, dateDuration, dateTickValues, axisBottomFormat }
}

export default useTrackerToLine
