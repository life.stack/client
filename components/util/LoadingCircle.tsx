import { CircularProgress } from '@chakra-ui/react'

const LoadingCircle = () => {
  return <CircularProgress />
}

export default LoadingCircle
