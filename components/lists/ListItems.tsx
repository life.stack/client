import { Stack, useColorModeValue } from '@chakra-ui/react'
import Item from './Item'

const ListItems = ({ items = [], toggleItemChecked, toggleSubitemChecked }) => {
  console.log('ListItems', items)
  return (
    <Stack width="full">
      {items.map((item) => {
        return (
          <Item
            key={item.id}
            item={item}
            toggleItemChecked={toggleItemChecked}
            toggleSubitemChecked={toggleSubitemChecked}
          />
        )
      })}
    </Stack>
  )
}

export default ListItems
