import { Input } from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { useEffect } from 'react'

const CreateItem = ({ appendItem, ...rest }) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { isSubmitSuccessful },
  } = useForm({
    defaultValues: { title: '' },
  })

  useEffect(() => {
    if (isSubmitSuccessful) {
      reset({ title: '' })
    }
  }, [isSubmitSuccessful, reset])

  return (
    <form
      onSubmit={handleSubmit(appendItem)}
      aria-label="add a new item to the list"
    >
      <Input
        {...register('title')}
        aria-label="title"
        placeholder="New Item"
        {...rest}
      />
    </form>
  )
}

export default CreateItem
