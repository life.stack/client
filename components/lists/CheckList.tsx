import {
  Divider,
  Flex,
  Heading,
  Stack,
  useColorModeValue,
} from '@chakra-ui/react'
import CreateItem from './CreateItem'
import ListItems from './ListItems'
import ListToolbar from './ListToolbar'
import useList from '../../api/useList'

const CheckList = ({ listId }) => {
  const {
    list,
    showChecked,
    toggleShowChecked,
    appendItem,
    filteredListItems,
    toggleItemChecked,
    toggleSubitemChecked,
    deleteList,
  } = useList(listId)

  return (
    <Flex>
      <Stack
        boxShadow={'2xl'}
        bg={useColorModeValue('white', 'gray.700')}
        rounded={[null, 'xl']}
        p={[4, 10]}
        spacing={4}
        align={'center'}
      >
        <Heading>{list?.name}</Heading>
        <Divider />
        <CreateItem appendItem={appendItem} />
        <ListItems
          items={filteredListItems}
          toggleItemChecked={toggleItemChecked}
          toggleSubitemChecked={toggleSubitemChecked}
        />
      </Stack>
      <ListToolbar
        toggleShowChecked={toggleShowChecked}
        showChecked={showChecked}
        deleteList={deleteList}
        list={list}
      />
    </Flex>
  )
}

export default CheckList
