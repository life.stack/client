import { IconButton, Tooltip } from '@chakra-ui/react'
import { MdDelete } from 'react-icons/md'
import { useState } from 'react'
import DeleteListAlert from '../DeleteListAlert'

const DeleteListButton = ({ deleteList, listName }) => {
  const [isOpen, setIsOpen] = useState(false)
  const onCancel = () => setIsOpen(false)
  const onDelete = () => {
    deleteList()
    onCancel()
  }

  return (
    <Tooltip label="Delete List">
      <>
        <IconButton
          aria-label="Delete List"
          icon={<MdDelete />}
          onClick={() => setIsOpen(true)}
        />
        <DeleteListAlert
          isOpen={isOpen}
          onCancel={onCancel}
          onDelete={onDelete}
          listName={listName}
        />
      </>
    </Tooltip>
  )
}

export default DeleteListButton
