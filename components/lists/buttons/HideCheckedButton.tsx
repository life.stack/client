import { IconButton, Tooltip } from '@chakra-ui/react'
import { BiHide, BiShow } from 'react-icons/bi'

const HideCheckedButton = ({ showChecked, toggleShowChecked }) => {
  const label = showChecked ? 'Hide Checked Items' : 'Show Checked Items'
  const Icon = showChecked ? <BiHide /> : <BiShow />

  return (
    <Tooltip label={label}>
      <IconButton aria-label={label} icon={Icon} onClick={toggleShowChecked} />
    </Tooltip>
  )
}

export default HideCheckedButton
