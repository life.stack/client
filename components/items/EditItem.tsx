import { Stack, useColorModeValue } from '@chakra-ui/react'
import useItem from '../../api/useItem'
import EditItemForm from './EditItemForm'

const EditItem = ({ itemId, initialItem }) => {
  const { item, updateItem, appendSubitem, toggleSubitemChecked } = useItem({
    itemId,
    initialItem,
  })

  return (
    <Stack
      direction={'column'}
      boxShadow={'2xl'}
      bg={useColorModeValue('white', 'gray.700')}
      rounded={'xl'}
      p={10}
      spacing={8}
    >
      <EditItemForm
        item={item}
        updateItem={updateItem}
        appendSubitem={appendSubitem}
        toggleSubitemChecked={toggleSubitemChecked}
      />
    </Stack>
  )
}

export default EditItem
