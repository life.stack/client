import { BsCalendar } from 'react-icons/bs'

const CalendarsIcon = (props) => {
  return <BsCalendar {...props} />
}

export default CalendarsIcon
