import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  HStack,
  Input,
  VStack,
  Text,
  Heading,
  InputGroup,
  InputLeftAddon,
  FormHelperText,
  Checkbox,
  Tooltip,
} from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { DevTool } from '@hookform/devtools'
import { useRouter } from 'next/router'
import { format, isDate, parse, parseISO } from 'date-fns'
import { IoLocationSharp } from 'react-icons/io5'
import {
  TiArrowLoop,
  TiMediaPlay,
  TiMediaPlayOutline,
  TiMediaStopOutline,
  TiTime,
} from 'react-icons/ti'
import useCalendarEvent from '../../../api/useCalendarEvent'
import ReoccuringEventSelect from './ReoccuringEventSelect'

const CreateCalendarEventForm = ({
  selectedStartDate,
  selectedEndDate,
  calendarId,
}) => {
  const router = useRouter()
  const { createCalendarEvent } = useCalendarEvent(calendarId)

  const {
    register,
    control,
    handleSubmit,
    formState: { errors, isDirty, isValid },
    getValues,
    watch,
  } = useForm({
    defaultValues: {
      title: '',
      location: '',
      start:
        (Array.isArray(selectedStartDate)
          ? selectedStartDate[0]
          : selectedStartDate) || new Date().toISOString(),
      end:
        (Array.isArray(selectedEndDate)
          ? selectedEndDate[0]
          : selectedEndDate) || new Date().toISOString(),
      allDay: true,
      reoccur: '',
    },
  })

  console.log('CreateCalendarEventForm', getValues('start'), getValues('end'))
  console.log('CreateCalendarEventForm', watch('start'), watch('reoccur'))
  // @ts-ignore
  const longFormat = format(parseISO(getValues('start')), 'PPPP')

  const onSubmit = (values) => {
    createCalendarEvent(values).then(() =>
      router.push(`/calendars/${calendarId}`)
    )
  }

  return (
    <VStack
      as="form"
      onSubmit={handleSubmit(onSubmit)}
      aria-label="Add an event to the calendar"
    >
      <HStack justify="space-between" width="100%">
        <Heading as="h2">{longFormat}</Heading>
        <Button type="submit" colorScheme="green" size="lg">
          Save
        </Button>
      </HStack>

      <Input
        {...register('title')}
        id="title"
        aria-label="title"
        placeholder="Enter title"
      />
      <InputGroup>
        <InputLeftAddon>
          <IoLocationSharp />
        </InputLeftAddon>
        <Input
          {...register('location')}
          id="location"
          aria-label="location"
          placeholder="Add location"
        />
      </InputGroup>
      <InputGroup>
        <InputLeftAddon>
          <TiTime />
        </InputLeftAddon>
        <Checkbox
          {...register('allDay')}
          id="allDay"
          aria-label="allDay"
          placeholder="allDay"
          size="lg"
          marginLeft="3"
        >
          All Day
        </Checkbox>
      </InputGroup>
      <InputGroup>
        <InputLeftAddon>
          <TiMediaPlayOutline />
        </InputLeftAddon>
        <Input
          {...register('start')}
          id="start"
          aria-label="start time"
          placeholder="Start time"
          type={watch('allDay') ? 'date' : 'datetime-local'}
        />
      </InputGroup>

      <Tooltip label="This day is exclusive">
        <InputGroup>
          <InputLeftAddon>
            <TiMediaStopOutline />
          </InputLeftAddon>
          <Input
            {...register('end')}
            id="end"
            aria-label="end time"
            placeholder="End time"
            type={watch('allDay') ? 'date' : 'datetime-local'}
          />
        </InputGroup>
      </Tooltip>

      <InputGroup>
        <InputLeftAddon>
          <TiArrowLoop />
        </InputLeftAddon>

        <ReoccuringEventSelect
          {...register('reoccur')}
          id="reoccur"
          aria-label="reoccurring event"
          placeholder="one time event"
          start={watch('start')}
          reoccur={getValues('reoccur')}
        />
      </InputGroup>
      {/*<FormControl isInvalid={!!errors?.start}>*/}
      {/*  <FormLabel htmlFor="start">Some Date</FormLabel>*/}
      {/*  <DateSingleInput ref={register} id="start" name="start" />*/}
      {/*  <FormErrorMessage>{errors?.start?.message}</FormErrorMessage>*/}
      {/*</FormControl>*/}

      {/*<FormControl>*/}
      {/*  <FormLabel>Date Range</FormLabel>*/}

      {/*  <DateRangeInput startRef={register} endRef={register} />*/}
      {/*  <FormHelperText>Text here</FormHelperText>*/}
      {/*</FormControl>*/}
      <DevTool control={control} />
    </VStack>
  )
}

export default CreateCalendarEventForm
