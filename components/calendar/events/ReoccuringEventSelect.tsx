import {
  Collapse,
  forwardRef,
  Heading,
  HStack,
  List,
  ListItem,
  Select,
  Slide,
  VStack,
} from '@chakra-ui/react'
import { format, parseISO } from 'date-fns'
import { useState } from 'react'

const ReoccuringEventSelect = forwardRef(({ start, ...props }, ref) => {
  const [selectValue, setSelectValue] = useState(null)
  console.log('ReoccuringEventSelect', props)
  const startDate = parseISO(start)

  const onSelect = (event) => {
    console.log(event.target.value)
    setSelectValue(event.target.value)
  }

  return (
    <VStack width="100%">
      <Select {...props} ref={ref} onChange={onSelect}>
        <option value="Daily">Daily</option>
        <option value="Weekly">Weekly on {format(startDate, 'EEEE')}</option>
        <option value="Monthly">
          Monthly on the {format(startDate, 'do')}
        </option>
        <option value="Annually">
          Annually on {format(startDate, 'MMMM do')}
        </option>
        <option value="Custom">Custom...</option>
      </Select>
      <Collapse in={selectValue === 'Custom'}>
        <VStack>
          <Heading size="sm">Frequency</Heading>
          <HStack>
            <List>
              <ListItem>1</ListItem>
              <ListItem>2</ListItem>
              <ListItem>3</ListItem>
              <ListItem>4</ListItem>
            </List>
            <List>
              <ListItem>Day</ListItem>
              <ListItem>Week</ListItem>
              <ListItem>Month</ListItem>
              <ListItem>Year</ListItem>
            </List>
          </HStack>
        </VStack>
      </Collapse>
    </VStack>
  )
})

export default ReoccuringEventSelect
