import {
  Button,
  Heading,
  Icon,
  Input,
  Stack,
  Text,
  useColorModeValue,
  VStack,
} from '@chakra-ui/react'
import CalendarsIcon from './CalendarsIcon'
import React from 'react'
import { useForm } from 'react-hook-form'
import useCalendar from '../../api/useCalendars'

const CreateCalendarForm = () => {
  const { register, handleSubmit } = useForm()
  const { createCalendar } = useCalendar()

  return (
    <Stack
      boxShadow={'2xl'}
      bg={useColorModeValue('white', 'gray.700')}
      rounded={'xl'}
      p={10}
      spacing={8}
      align={'center'}
    >
      <Icon as={CalendarsIcon} w={24} h={24} />
      <Stack align={'center'} spacing={2}>
        <Heading
          textTransform={'uppercase'}
          fontSize={'3xl'}
          color={useColorModeValue('gray.800', 'gray.200')}
        >
          Calendar
        </Heading>
        <Text fontSize={'lg'} color={'gray.500'}>
          Give the calendar a name
        </Text>
      </Stack>
      <Stack
        as="form"
        onSubmit={handleSubmit(createCalendar)}
        spacing={4}
        direction={{ base: 'column', md: 'row' }}
        width="full"
      >
        <VStack>
          <Input
            {...register('name')}
            aria-label="give the calendar a name"
            type="text"
            placeholder="Calendar Name"
            color={useColorModeValue('gray.800', 'gray.200')}
            bg={useColorModeValue('gray.100', 'gray.600')}
            rounded="full"
            border={0}
            _focus={{
              bg: useColorModeValue('gray.200', 'gray.800'),
              outline: 'none',
            }}
          />
        </VStack>

        <Button
          type="submit"
          colorScheme="green"
          rounded="full"
          flex="1 0 auto"
          _hover={{ bg: 'blue.500' }}
          _focus={{ bg: 'blue.500' }}
        >
          Create
        </Button>
      </Stack>
    </Stack>
  )
}

export default CreateCalendarForm
