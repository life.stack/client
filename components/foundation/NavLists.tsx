import { Collapse, Icon, useDisclosure } from '@chakra-ui/react'
import NavItem from './NavItem'
import { MdKeyboardArrowRight, MdPlaylistAdd } from 'react-icons/md'
import useLists from '../../api/useLists'
import { FaClipboardCheck } from 'react-icons/fa'

const NavLists = ({}) => {
  const integrations = useDisclosure()
  const { navBarLists } = useLists()

  return (
    <>
      <NavItem icon={FaClipboardCheck} onClick={integrations.onToggle}>
        Lists
        <Icon
          as={MdKeyboardArrowRight}
          ml="auto"
          transform={integrations.isOpen && 'rotate(90deg)'}
        />
      </NavItem>
      <Collapse in={integrations.isOpen}>
        <NavItem pl="12" py="2" icon={MdPlaylistAdd} href="/lists/new">
          Create a new list
        </NavItem>
        {Array.isArray(navBarLists) &&
          navBarLists.map((list) => {
            // console.log('NavList', list)
            return (
              <NavItem key={list.id} pl="12" py="2" href={`/lists/${list.id}`}>
                {list.name}
              </NavItem>
            )
          })}
      </Collapse>
    </>
  )
}

export default NavLists
