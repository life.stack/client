import { Collapse, Icon, useDisclosure } from '@chakra-ui/react'
import NavItem from './NavItem'
import { MdKeyboardArrowRight, MdPlaylistAdd } from 'react-icons/md'
import useTrackers from '../../api/useTrackers'
import TrackersIcon from '../trackers/TrackersIcon'

const NavTrackers = ({}) => {
  const integrations = useDisclosure()
  const { navBarTrackers } = useTrackers()

  return (
    <>
      <NavItem icon={TrackersIcon} onClick={integrations.onToggle}>
        Trackers
        <Icon
          as={MdKeyboardArrowRight}
          ml="auto"
          transform={integrations.isOpen && 'rotate(90deg)'}
        />
      </NavItem>
      <Collapse in={integrations.isOpen}>
        <NavItem pl="12" py="2" icon={MdPlaylistAdd} href="/trackers/new">
          Create a new tracker
        </NavItem>
        {Array.isArray(navBarTrackers) &&
          navBarTrackers.map((tracker) => {
            return (
              <NavItem
                key={tracker.id}
                pl="12"
                py="2"
                href={`/trackers/${tracker.id}`}
              >
                {tracker.name}
              </NavItem>
            )
          })}
      </Collapse>
    </>
  )
}

export default NavTrackers
