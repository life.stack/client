import { IconButton } from '@chakra-ui/react'
import { MdPlaylistAdd } from 'react-icons/md'

const CreateListButton = () => {
  return (
    <IconButton
      aria-label="create a new list"
      icon={<MdPlaylistAdd />}
      colorScheme="gray"
      marginLeft="1em"
    />
  )
}

export default CreateListButton
