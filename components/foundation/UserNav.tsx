import {
  Avatar,
  Button,
  Menu,
  MenuButton,
  MenuDivider,
  MenuItem,
  MenuList,
} from '@chakra-ui/react'
import { useRouter } from 'next/router'
import { GrUser } from 'react-icons/gr'

const UserNav = ({ currentUser }) => {
  const router = useRouter()

  return (
    <Menu>
      <MenuButton as={Button} rounded="full" variant="link" cursor="pointer">
        {currentUser ? (
          <Avatar
            ml="4"
            size="sm"
            name="fubz"
            src="https://avatars.githubusercontent.com/u/6692096?v=4"
            cursor="pointer"
          />
        ) : (
          <Avatar ml="4" size="sm" icon={<GrUser />} cursor="pointer" />
        )}
      </MenuButton>
      <MenuList>
        {!currentUser && (
          <MenuItem onClick={() => router.push('/auth/signin')}>
            Sign in
          </MenuItem>
        )}
        {!currentUser && (
          <MenuItem onClick={() => router.push('/auth/signup')}>
            Sign up
          </MenuItem>
        )}
        {currentUser && (
          <>
            <MenuItem onClick={() => router.push('/profile/currentUser')}>
              Profile
            </MenuItem>
            <MenuDivider />
            <MenuItem onClick={() => router.push('/auth/signout')}>
              Sign out
            </MenuItem>
          </>
        )}
      </MenuList>
    </Menu>
  )
}

export default UserNav
