import { GrGroup } from 'react-icons/gr'

const GroupIcon = (props) => {
  return <GrGroup {...props} />
}

export default GroupIcon
