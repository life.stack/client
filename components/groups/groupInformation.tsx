import {
  Avatar,
  Box,
  Button,
  chakra,
  Flex,
  FormControl,
  FormLabel,
  GridItem,
  Icon,
  Input,
  InputGroup,
  InputLeftAddon,
  SimpleGrid,
  Stack,
  useColorModeValue,
} from '@chakra-ui/react'
import { FaUser } from 'react-icons/fa'
import React from 'react'

const GroupInformation = () => {
  return (
    <chakra.form
      method="POST"
      shadow="base"
      rounded={[null, 'md']}
      overflow={{ sm: 'hidden' }}
    >
      <Stack
        px={4}
        py={5}
        bg={useColorModeValue('white', 'gray.700')}
        spacing={6}
        p={{ sm: 6 }}
      >
        <div>
          <FormControl id="email" mt={1}>
            <FormLabel
              fontSize="sm"
              fontWeight="md"
              color={useColorModeValue('gray.700', 'gray.50')}
            >
              Group Name
            </FormLabel>
            <Input
              placeholder="Our Family"
              shadow="sm"
              focusBorderColor="brand.400"
              fontSize={{ sm: 'sm' }}
            />
          </FormControl>
        </div>

        <SimpleGrid columns={3} spacing={6}>
          <FormControl as={GridItem} colSpan={[3, 2]}>
            <FormLabel
              fontSize="sm"
              fontWeight="md"
              color={useColorModeValue('gray.700', 'gray.50')}
            >
              Website
            </FormLabel>
            <InputGroup size="sm">
              <InputLeftAddon
                bg={useColorModeValue('gray.50', 'gray.800')}
                color={useColorModeValue('gray.500', 'gay.50')}
                rounded="md"
              >
                https://
              </InputLeftAddon>
              <Input
                type="tel"
                placeholder="www.example.com"
                focusBorderColor="brand.400"
                rounded="md"
              />
            </InputGroup>
          </FormControl>
        </SimpleGrid>

        <FormControl>
          <FormLabel
            fontSize="sm"
            fontWeight="md"
            color={useColorModeValue('gray.700', 'gray.50')}
          >
            Photo
          </FormLabel>
          <Flex alignItems="center" mt={1}>
            <Avatar
              boxSize={12}
              bg={useColorModeValue('gray.100', 'gray.800')}
              icon={
                <Icon
                  as={FaUser}
                  boxSize={9}
                  mt={3}
                  rounded="full"
                  color={useColorModeValue('gray.300', 'gray.700')}
                />
              }
            />
            <Button
              type="button"
              ml={5}
              variant="outline"
              size="sm"
              fontWeight="medium"
              _focus={{ shadow: 'none' }}
            >
              Change
            </Button>
          </Flex>
        </FormControl>
      </Stack>
      <Box
        px={{ base: 4, sm: 6 }}
        py={3}
        bg={useColorModeValue('gray.50', 'gray.900')}
        textAlign="right"
      >
        <Button
          type="submit"
          colorScheme="brand"
          _focus={{ shadow: '' }}
          fontWeight="md"
        >
          Save
        </Button>
      </Box>
    </chakra.form>
  )
}

export default GroupInformation
