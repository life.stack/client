import CreateCalendarEventForm from '../../../../components/calendar/events/CreateCalendarEventForm'
import { useRouter } from 'next/router'

const IndexEventsPage = (props) => {
  const router = useRouter()
  const { selectedStartDate, selectedEndDate } = router.query

  return (
    <CreateCalendarEventForm
      selectedStartDate={
        selectedStartDate ? selectedStartDate : props.selectedStartDate
      }
      selectedEndDate={
        selectedEndDate ? selectedEndDate : props.selectedStartDate
      }
      calendarId={props.calendarId}
    />
  )
}

export async function getServerSideProps(context) {
  const { selectedStartDate, selectedEndDate } = context.query
  const { calendarId } = context.params

  return { props: { selectedStartDate, selectedEndDate, calendarId } }
}

export default IndexEventsPage
