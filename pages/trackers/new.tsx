import React from 'react'
import { useColorModeValue, Wrap } from '@chakra-ui/react'
import BaseType from '../../components/trackers/types/BaseType'
import WeightTrackerType from '../../components/trackers/types/WeightTrackerType'
import TemperatureTrackerType from '../../components/trackers/types/TemperatureTrackerType'

const NewTracker = () => {
  return (
    <Wrap
      align="center"
      justify="space-around"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <WeightTrackerType />
      <TemperatureTrackerType />
      <BaseType />
    </Wrap>
  )
}

export async function getServerSideProps(context) {
  if (!context.req.headers.cookie) {
    return {
      props: {},
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }

  return {
    props: {},
  }
}

export default NewTracker
