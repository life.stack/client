import React from 'react'
import {
  Box,
  Button,
  chakra,
  Flex,
  GridItem,
  Icon,
  Input,
  SimpleGrid,
  Stack,
  useColorModeValue,
} from '@chakra-ui/react'
import { useGroups } from '../../api/useGroups'
import { useForm } from 'react-hook-form'

const CreateGroup = () => {
  const { groups, createGroup, isLoading } = useGroups()
  const { register, handleSubmit } = useForm()
  const Feature = (props) => (
    <Flex alignItems="center" color={useColorModeValue(null, 'white')}>
      <Icon
        boxSize={4}
        mr={1}
        color="green.600"
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
          clipRule="evenodd"
        />
      </Icon>
      {props.children}
    </Flex>
  )
  return (
    <Box px={4} py={32} mx="auto">
      <Box
        w={{ base: 'full', md: 11 / 12, xl: 8 / 12 }}
        textAlign={{ base: 'left', md: 'center' }}
        mx="auto"
      >
        <chakra.h1
          mb={3}
          fontSize={{ base: '4xl', md: '5xl' }}
          fontWeight={{ base: 'bold', md: 'extrabold' }}
          color={useColorModeValue('gray.900', 'gray.100')}
          lineHeight="shorter"
        >
          Whether a Family, Team, or any other group
        </chakra.h1>
        <chakra.p
          mb={6}
          fontSize={{ base: 'lg', md: 'xl' }}
          color="gray.500"
          lineHeight="base"
        >
          Start by creating a name for yourselves. Then begin tracking and
          stacking away at those life habits.
        </chakra.p>
        <SimpleGrid
          as="form"
          onSubmit={handleSubmit(createGroup)}
          w={{ base: 'full', md: 7 / 12 }}
          columns={{ base: 1, lg: 6 }}
          spacing={3}
          pt={1}
          mx="auto"
          mb={8}
        >
          <GridItem as="label" colSpan={{ base: 'auto', lg: 4 }}>
            <Input
              mt={0}
              size="lg"
              type="name"
              placeholder="Enter your group name..."
              required
              {...register('name', { required: true })}
            />
          </GridItem>
          <GridItem w="full" colSpan={{ base: 'auto', lg: 2 }}>
            <Button
              type="submit"
              variant="solid"
              size="lg"
              colorScheme="brand"
              isLoading={isLoading}
            >
              Get Started
            </Button>
          </GridItem>
        </SimpleGrid>
        <Stack
          display="flex"
          direction={{ base: 'column', md: 'row' }}
          justifyContent={{ base: 'start', md: 'center' }}
          mb={3}
          spacing={{ base: 2, md: 8 }}
          fontSize="xs"
          color="gray.600"
        >
          <Feature>Todo Lists</Feature>
          <Feature>Calendar</Feature>
          <Feature>Workout Tracking</Feature>
          <Feature>Food Diary</Feature>
          <Feature>Habit Tracking</Feature>
        </Stack>
      </Box>
    </Box>
  )
}

export default CreateGroup
