import { Flex, useColorModeValue } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import LoadingCircle from '../../../../components/util/LoadingCircle'
import EditItem from '../../../../components/items/EditItem'
import serverFetcher from '../../../../api/serverFetcher'

const EditItemPage = ({ initialItem }) => {
  const router = useRouter()
  const { itemId } = router.query

  return (
    <Flex
      align="center"
      justify="center"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      {itemId ? (
        <EditItem itemId={itemId} initialItem={initialItem} />
      ) : (
        <LoadingCircle />
      )}
    </Flex>
  )
}

export async function getServerSideProps(context) {
  const { itemId } = context.query
  const data = await serverFetcher({
    uri: `/api/items/${itemId}`,
    req: context.req,
  })
  console.log('item.edit', data)

  return {
    props: { initialItem: data }, // will be passed to the page component as props
  }
}

export default EditItemPage
