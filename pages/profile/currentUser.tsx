import { useUsers } from '../../api/useUsers'
import { Box } from '@chakra-ui/react'

const CurrentUser = () => {
  const { user } = useUsers()

  return <Box>{JSON.stringify(user)}</Box>
}

export default CurrentUser
