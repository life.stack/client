import { useEffect } from 'react'
import { useUsers } from '../../api/useUsers'
import { useGroups } from '../../api/useGroups'
import Router from 'next/router'

const Signout = () => {
  const { signout } = useUsers()
  const { clearGroups } = useGroups()

  useEffect(() => {
    const reset = async () => {
      await signout()
      await clearGroups()
      await Router.push('/')
    }
    reset().then()
  })

  return <div>Signing you out...</div>
}

export default Signout
