import Signin from '../../components/auth/signin'

const SigninPage = () => {
  return <Signin />
}

export default SigninPage
